# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include starbucks_aplicacao
class starbucks_aplicacao {
exec { 'refresh_aplicacao':
  command => "/usr/bin/systemctl restart aplicacao",
  refreshonly => true,
}
exec { 'refresh_daemon_1':
  command => "/usr/bin/systemctl daemon-reload",
  refreshonly => true,
}
group { 'java':
       ensure => 'present',
       gid    => '501',
     }
user { 'java':
       ensure           => 'present',
       gid              => '501',
       home             => '/home/java',
       password         => '!!',
       password_max_age => '99999',
       password_min_age => '0',
       shell            => '/bin/bash',
       uid              => '501',
     }
file { '/opt/apps':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }
file { '/opt/apps/aplicacao':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }
file { "/etc/systemd/system/aplicacao.service": 
        mode => "0644",
        owner => 'root',
        group => 'root',
        source => 'puppet:///modules/starbucks_aplicacao/aplicacao.service',
        notify => Exec['refresh_daemon_1'],
     }
package { 'maven':
        ensure => installed,
        name   => $maven,
    }
remote_file { "/opt/apps/aplicacao/application.yml":
        mode => "0644",
        owner => 'java',
        group => 'java',
        source => 'http://23.96.48.125/artfactory/grupo2/aplicacao/application.yml',
        notify => Exec['refresh_aplicacao'],
    }
remote_file { '/opt/apps/aplicacao/aplicacao.jar':
    ensure => latest,
    owner => 'java',
    group => 'java',
    source => 'http://23.96.48.125/artfactory/grupo2/aplicacao-grupo2.jar',
    notify => Exec['refresh_aplicacao'],
  }
}
